![alt strefakursow.pl](https://bedeprogramistka.pl/wp-content/uploads/2018/08/strefa-kursow-logo-1.jpg "Strefa Kursów Szkolenia Online")

# gitlab_sk_zaaw_01

**Opis:**
Jest to repozytorium powiązane z kursem na stronie Strefa Kursów
dostępne pod tym [adresem](https://example.strefakursow.pl)

## Nasza Check lista do kursu

1. [x] Wprowadzenie
    1. [x] Wstęp 01m 40s
    2. [x] Jak korzystać z materiałów 01m 14s
    3. [x] Krótkie omówienie projektów 02m 42s
2. [x] Instalacja i konfiguracja Git
    1. [x] Windows 01m 15s
    2. [x] Visual Studio Code 02m 04s
3. [x] Git Gitlab to musisz wiedzieć
    1. [x] Pierwszy projekt w GitLab i git clone 03m 56s
    2. [x] Git commit-pierwsze zmiany w repo 11m 26s
    3. [x] Markdown-nasz plik README.md 15m 45s
    4. [x] Gitignore-ignorujemy pliki 11m 30s
    5. [x] Windows Subsystem for Linux 06m 05s
    6. [x] Gitlab-nowy branch 07m 49s
    7. [x] Merge request-żadanie scalenia 10m 02s
    8. [x] Staging i commiting w Web IDE-rejestr zmian w repo 05m 58s
4. [x] Polecenia w Git
    1. [ ] Git pull i commit 09m 26s
    2. [x] Tagi 04m 26s
    3. [x] Branche 08m 29s
    4. [x] Git push tags 04m 22s
    5. [x] Merge branchy 19m 30s
    6. [ ] Rebasing
    7. [x] Reverting a commit-odwracanie zmian 04m 20s
    8. [x] Komenda diff 03m 14s
    9. [x] Garbage Collection w Git 01m 56s
5. [x] Zarządzanie branchami i tagowanie
    1. [ ] Force push na zdalne repo 13m 11s
    2. [x] Identyfikacja branchy 04m 15s
    3. [x] Usuwanie lokalnego i zdalnego brancha 08m 38s
    4. [x] Prune stale branch-usuwanie info o śledz zdalnym branchu 10m 44s
    5. [x] Git log-logi commitów 08m 40s
    6. [x] Tagging-tworz ważnych punktów historii kodu 09m 37s
6. [x] Interactive Staging-interaktywne polecenia Git
    1. [x] Interactive mode-tryb interaktywny 06m 21s
    2. [ ] Patch mode-tworz hunków co ma być śledzone 09m 41s
    3. [ ] Split hunk-dzielenie hunków 02m 26s
    4. [ ] Edytowanie hunków 01m 44s
7. [ ] Wybór udostepnionych zmian
    1. [ ] Cherry-picking commits wybier i zaciąg brancha do nowego commita 03m 31s
    2. [ ] Cherry-picking-rozwiązywanie problemów 06m 11s
    3. [ ] Diff patches-usuwanie różnic w plikach 03m 51s
    4. [ ] Wykorzystanie otrzymanych diff patches 04m 08s
    5. [ ] Rebase-integracja zmian 02m 37s
8. [ ] GitLab server
    1. [ ] Instalacja 08m 30s
    2. [ ] Pracujemy z własnym GitLab Server 05m 25s
9. [ ] GitLab server Zakonczenie 01m 32s
10. [ ] Kurs Git dla zaawansowanych-test

## Powiązane kursy

| NAZWA KURSU                    | ADRES URL                                                                       | POZIOM KURSU        |
| ------------------------------ | ------------------------------------------------------------------------------- | ------------------- |
| Dobre praktyki pracy w zespole | https://strefakursow.pl/kursy/programowanie/dobre_praktyki_pracy_w_zespole.html | Średniozaawansowany |
| Kurs Git dla poczatkujących    | https://strefakursow.pl/kursy/programowanie/kurs_git_dla_poczatkujacych.html    | Podstawowy          |

## AG

## GA

## DB
